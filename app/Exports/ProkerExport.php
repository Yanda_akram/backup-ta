<?php

namespace App\Exports;

use App\Models\Proker;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class ProkerExport implements FromView
{
    /**
    * @return \Illuminate\Support\view
    */
    public function view(): View
    {
        $proker = Proker::join('pengajuan as p','p.id','=','proker.id_pengajuan')
        ->selectRaw("proker.*, p.id as pengajuan_id,nama_proker,tempat,anggaran")
        // ->toSql();
        ->get();
        
        return view('ketua.excelproker', [
            'proker' => $proker
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rapat extends Model
{
    use HasFactory;
    //menggunakan softdelete
    use SoftDeletes;
    //nama tabel yang akan digunakan yaitu tabel rapat
    protected $table = 'rapat';

    //kolom tabel yang boleh diisi
    protected $fillable = ['id','id_proker','tanggal','tempat','tema','jumlah_peserta','penyaji','susunan_acara','catatan'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proker extends Model
{
    use HasFactory;

     //menggunakan softdelete
     use SoftDeletes;

     //nama tabel yang akan digunakan yaitu tabel proker
     protected $table = 'proker';
     
     //kolom tabel yang boleh diisi
     protected $fillable = ['id','id_pengajuan','tanggal','status'];
 
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pengajuan extends Model
{
    use HasFactory;

    //menggunakan softdelete
    use SoftDeletes;

    //nama tabel yang akan digunakan yaitu tabel pengajuan
    protected $table = 'pengajuan';
    
    //kolom tabel yang boleh diisi
    protected $fillable = ['id','id_user','nama_proker','tempat','sumber_dana','anggaran',
    'detail','status','keterangan','catatan'];

}

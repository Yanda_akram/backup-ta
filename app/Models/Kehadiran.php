<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Kehadiran extends Model
{
    use HasFactory;


    //menggunakan softdelete
    use SoftDeletes;
    //nama tabel yang akan digunakan yaitu tabel rapat
    protected $table = 'kehadiran';

    //kolom tabel yang boleh diisi
    protected $fillable = ['id','id_user','id_rapat'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lpj extends Model
{
    use HasFactory;
    use SoftDeletes;

    //nama tabel yang akan digunakan yaitu tabel lpj
    protected $table = 'lpj';
    
    //kolom tabel yang boleh diisi
    protected $fillable = ['id','id_proker','lpj','status','keterangan'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Panitia extends Model
{
    use HasFactory;
    //menggunakan softdelete
    use SoftDeletes;

    //nama tabel yang akan digunakan yaitu tabel panitia
    protected $table = 'panitia';
    
    //kolom tabel yang boleh diisi
    protected $fillable = ['id','id_user','id_proker','jabatan','divisi'];
}

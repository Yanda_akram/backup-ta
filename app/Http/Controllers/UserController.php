<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datauser;
use Illuminate\Support\Facades\DB;
use Hash;
class UserController extends Controller
{
   //membuat fungsi untuk menampilkan data dari database ke dalam laravel
   public function index()
   {
       //mengambil data pengajuan dengan fungsi all (semua data)
       $user = Datauser::all();
      //  $pengajuan = Pengajuan::where('status','diterima')->get();

       //menampilkan data ke view anggota/pengajuan 
       return view('ketua.datauser', compact('user'));
   }
   //membuat fungsi untuk beralir ke halaman form tambah pengajuan
   public function tambah()
   {
       //menampilkan halaman form addpengajuan
       return view('ketua.adddatauser');
   }

   //fungsi untuk membuat proses inputan data
   public function store(Request $request)
   {
       //validasi untuk mengisi kolom
    //    return $request->all();
       $this->validate($request,[
           'name' => 'required',
           'username' => 'required',
           'password' => 'required',
           'level' => 'required',
       ]);
       //fungsi untuk proses inputan data ke database

       foreach ($request->name as $key => $value) {
           Datauser::create([
               'name' => $request->name[$key],
               'username' => $request->username[$key],
               'password' => Hash::make($request->password[$key]),
               'level' => $request->level[$key],
  
           ]);
       }
       return redirect('ketua/user')
       ->with('createdpanitia','Data User Berhasil Ditambahkan');
   }

   public function edit($id)
    {
        $user = DB::table('user')->where('id',$id)->get();
        return view('ketua.editdatauser', compact('user'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Datauser $user)
    {
        DB::table('user')->where('id',$request->id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
            'level' => $request->level,
        ]);

        // rapat::find($request->id);
        return redirect('ketua/user');
        // return $request->all();
        //->with('upd-user','Data User Berhasil Diupdate!!!')
    }


     public function hapus($id)
     {
         Datauser::where('id',$id)->delete();
        //  return back()->with('deleted','berhasil terhapus!!!');
         return back()->with('del-rapat','berhasil terhapus!!!');
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lpj;
use Illuminate\Support\Facades\DB;
use DataTables;

class LpjController extends Controller
{
 // ###################### LPJ ANGGOTA ########################################
     //membuat fungsi untuk menampilkan data dari database ke dalam laravel
     public function index()
     {
         //mengambil data pengajuan dengan fungsi all (semua data)
         $lpj = Lpj::all();
        //  $pengajuan = Pengajuan::where('status','diterima')->get();
 
         //menampilkan data ke view anggota/pengajuan 
         return view('anggota.lpj', compact('lpj'));
     }

     public function upload(){
		$lpj = Lpj::all();
		return view('anggota.addlpj',['lpj' => $lpj]);
	}
 
	public function proses_upload(Request $request){
		$this->validate($request, [
			'lpj' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('lpj');
 
		$nama_file = time()."_".$file->getClientOriginalName();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'dokumen';
		$file->move($tujuan_upload,$nama_file);
 
		Lpj::create([
			'lpj' => $nama_file,

		]);
		// DB::table('lpj')->where('id',$request->id)->update([
        //     'lpj' => $request->$nama_file,
        //     'id_proker' => $request->id_proker,

        // ]);
 
		// return view('anggota.lpj');
		return redirect()->back();
	}
	// public function edit($id)
    // {
    //     $id_edit = $id;
    //     $lpj = DB::table('lpj')->where('id',$id)->get();
       
	// 	return view('anggota.addlpj', compact('lpj','id_edit'));
	// 	// return $lpj;
	// }


	// public function uploadfile($id){
	// 	$lpj = Lpj::get();
	// 	return view('anggota.addlpj',['lpj' => $lpj]);
	// }





}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Panitia;
use Illuminate\Support\Facades\DB;
use DataTables;

class PanitiaController extends Controller
{
    // -----------Awal - Panitia - Ketua--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
        //mengambil data pengajuan dengan fungsi all (semua data)
        $proker = DB::table('proker')
        ->join('pengajuan as p','p.id','=','proker.id_pengajuan')
        ->selectRaw("proker.id as proker_id, nama_proker");

        $panitia = Panitia::join('user as u', 'u.id','=','panitia.id_user')
        // ->join('proker as p', 'p.id', '=', 'panitia.id_proker')
        ->join(DB::raw('('.$proker->toSql().') pro'),'pro.proker_id','=','panitia.id_proker')
        ->selectRaw("panitia.*, u.id as user_id,name,nama_proker")
        ->get();
        // return $panitia;
        //menampilkan data ke view anggota/pengajuan 
    	return view('ketua.panitia', compact('panitia'));
    }


      //membuat fungsi untuk beralir ke halaman form tambah pengajuan
      public function tambah()
      {
          //menampilkan halaman form addpengajuan
          $user = DB::table('user')->selectRaw("id, name")->get();
          $data['user'] = $user;
          $proker = DB::table('proker')
          ->join('pengajuan as p','p.id','=','proker.id_pengajuan')
          ->whereRaw("proker.deleted_at is null")
          ->selectRaw("proker.id, nama_proker")->get();
          $data['proker'] = $proker;
        //   dd($proker);

          return view('ketua.addpanitia', $data);
      }
  
      //fungsi untuk membuat proses inputan data
      public function store(Request $request)
      {
          //validasi untuk mengisi kolom
        //   return $request->all();
          $this->validate($request,[
              'id_user' => 'required',
              'id_proker' => 'required',
              'jabatan' => 'required',
              'divisi' => 'required',
          ]);
          //fungsi untuk proses inputan data ke database

          foreach ($request->id_user as $key => $value) {
              Panitia::create([
                  'id' => $request->id[$key],
                  'id_user' => $request->id_user[$key],
                  'id_proker' => $request->id_proker[$key],
                  'jabatan' => $request->jabatan[$key],
                  'divisi' => $request->divisi[$key],
     
              ]);
          }
          return redirect('ketua/panitia')
          ->with('createdpanitia','Data Panitia Berhasil Ditambahkan');
      }

       //fungsi untuk menampilkan form edit
    public function edit($id)
    {
        $pengajuan = DB::table('pengajuan as pj')
        ->join('proker as pr','pr.id_pengajuan','=','pj.id')
        ->whereRaw("pj.deleted_at is null")
        ->selectRaw("pr.id, nama_proker")
        ->get();
        // return $pengajuan;
        $panitia = DB::table('panitia')->where('id',$id)->get();
        $user = DB::table('user')->selectRaw("id, name")->get();
        return view('ketua.editpanitia', compact('panitia','user','pengajuan'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Panitia $panitia)
    {
        // return $request->all();
        DB::table('panitia')->where('id',$request->id)->update([
            'id_user' => $request->id_user,
            'id_proker' => $request->id_proker,
            'jabatan' => $request->jabatan,
            'divisi' => $request->divisi
        ]);
        return redirect('ketua/panitia')
        ->with('updatedpanitia','Data Panitia 
        Berhasil Diperbarui');
     ;
    }

    //membuat fungsi untuk memproses hapus
    public function hapus($id)
    {
        Panitia::where('id',$id)->delete();
        return back()->with('deletedpanitia','Data Panitia Berhasil terhapus!!!');
    }

    // -----------Awal - Panitia - Anggota--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function indexanggota()
    {
        //mengambil data pengajuan dengan fungsi all (semua data)
        $proker = DB::table('proker')
        ->join('pengajuan as p','p.id','=','proker.id_pengajuan')
        ->selectRaw("proker.id as proker_id, nama_proker");

        $panitia = Panitia::join('user as u', 'u.id','=','panitia.id_user')
        // ->join('proker as p', 'p.id', '=', 'panitia.id_proker')
        ->join(DB::raw('('.$proker->toSql().') pro'),'pro.proker_id','=','panitia.id_proker')
        ->selectRaw("panitia.*, u.id as user_id,name,nama_proker")
        ->get();
        // return $panitia;
        //menampilkan data ke view anggota/pengajuan 
    	return view('anggota.panitia', compact('panitia'));
    }
}

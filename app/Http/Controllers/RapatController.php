<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rapat;
use Illuminate\Support\Facades\DB;

class RapatController extends Controller
{
    // ###################### RAPAT KETUA ########################################
     //membuat fungsi untuk menampilkan data dari database ke dalam laravel
     public function index()
     {
         //mengambil data pengajuan dengan fungsi all (semua data)
         $rapat = Rapat::all();
        //  $pengajuan = Pengajuan::where('status','diterima')->get();
 
         //menampilkan data ke view anggota/pengajuan 
         return view('ketua.rapat', compact('rapat'));
     }

     public function tambah()
     {
        //menampilkan halaman form addpengajuan
        return view('ketua.addrapat');
     }

      //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_proker' => 'required',
            'tanggal' => 'required',
            'tempat' => 'required',
            'tema' => 'required',
            'jumlah_peserta' => 'required',
            'penyaji' => 'required',
            'susunan_acara' => 'required',
            'catatan' => 'required',
        ]);
        //fungsi untuk proses inputan data ke database
        Rapat::create([
            'id' => $request->id,
            'id_proker' => $request->id_proker,
            'tanggal' => $request->tanggal,
            'tempat' => $request->tempat,
            'tema' => $request->tema,
            'jumlah_peserta' => $request->jumlah_peserta,
            'penyaji' => $request->penyaji,
            'susunan_acara' => $request->susunan_acara,
            'catatan' => $request->catatan,
        ]);
        return redirect('ketua/rapat')->with('add-rapat','Data Rapat Berhasil Ditambah!!!');
    }
    public function edit($id)
    {
        $rapat = DB::table('rapat')->where('id',$id)->get();
        return view('ketua.editrapat', compact('rapat'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Rapat $rapat)
    {
        DB::table('rapat')->where('id',$request->id)->update([
            'id_proker' => $request->id_proker,
            'tanggal' => $request->tanggal,
            'tempat' => $request->tempat,
            'tema' => $request->tema,
            'jumlah_peserta' => $request->jumlah_peserta,
            'penyaji' => $request->penyaji,
            'susunan_acara' => $request->susunan_acara,
            'catatan' => $request->catatan
        ]);

        // rapat::find($request->id);
        return redirect('ketua/rapat')->with('upd-rapat','Data Rapat Berhasil Diupdate!!!');
    }


     public function hapus($id)
     {
         Rapat::where('id',$id)->delete();
        //  return back()->with('deleted','berhasil terhapus!!!');
         return back()->with('del-rapat','berhasil terhapus!!!');
     }

     // ###################### RAPAT ANGGOTA ########################################
     //membuat fungsi untuk menampilkan data dari database ke dalam laravel
     public function indexanggota()
     {
         //mengambil data pengajuan dengan fungsi all (semua data)
         $rapat = Rapat::all();
        //  $pengajuan = Pengajuan::where('status','diterima')->get();
         //menampilkan data ke view anggota/pengajuan 
         return view('anggota.rapat', compact('rapat'));
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengajuan;
use Illuminate\Support\Facades\DB;
use Session;

class PengajuanController extends Controller
{
// -----------Awal - Pengajuan - Anggota--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
    $pengajuan = Pengajuan::join('user as u','u.id','=','pengajuan.id_user')
    ->selectRaw("pengajuan.*, u.id as user_id,name")
    ->get();
    // return $pengajuan;
    // $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view anggota/pengajuan 
    	return view('anggota.pengajuan', compact('pengajuan'));
    }

    //membuat fungsi untuk beralir ke halaman form tambah pengajuan
    public function tambah()
    {
        //menampilkan halaman form addpengajuan
        $user = DB::table('user')->selectRaw("id, name")->get();
        $data['user'] = $user;

        return view('anggota.addpengajuan', $data);
    }

    //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_user' => 'required',
            'nama_proker' => 'required',
            'detail' => 'required',
            'tempat' => 'required',
            'sumber_dana' => 'required',
            'anggaran' => 'required',
        ]);
        //fungsi untuk proses inputan data ke database
        Pengajuan::create([
            'id' => $request->id,
            'id_user' => $request->id_user,
            'nama_proker' => $request->nama_proker,
            'detail' => $request->detail,
            'tempat' => $request->tempat,
            'sumber_dana' => $request->sumber_dana,
            'anggaran' => $request->anggaran,
            'status' => $request->status,
            'keterangan' => $request->keterangan,
            'catatan' => $request->catatan,
        ]);
        return redirect('anggota/pengajuan')->with('pengangg1','Data Pengajuan Berhasil Ditambah!!!');
    }

// -----------Akhir - Pengajuan - Anggota--------------------------------------------------


// -----------Awal - Pengajuan - Ketua--------------------------------------------------
    public function indexket()
    {
    //mengambil data pengajuan dengan fungsi all (semua data)
    
    $pengajuan = Pengajuan::join('user as u','u.id','=','pengajuan.id_user')
    ->selectRaw("pengajuan.*, u.id as user_id,name")
    ->get();
    // return $pengajuan;
    // $pengajuan = Pengajuan::where('status','diterima')->get();

    //menampilkan data ke view anggota/pengajuan 
    return view('ketua.pengajuan', compact('pengajuan'));
    }

    //fungsi untuk menampilkan form edit
    public function editket($id)
    {
        $pengajuan = DB::table('pengajuan')->where('id',$id)->get();
        $user = DB::table('user')->selectRaw("id, name")->get();
        return view('ketua.editpengajuan', compact('pengajuan','user'));
    }
    //membuat fungsi untuk proses update
    public function updateket(Request $request, Pengajuan $pengajuan)
    {

        $session = Session::get('data_user');
        $level = $session['level'];
        $status = (isset($request->status)) ? $request->status : '';
        $keterangan = (isset($request->keterangan)) ? $request->keterangan : '';

        $data_update_pengajuan = [
            'id_user' => $request->id_user,
            'nama_proker' => $request->nama_proker,
            'detail' => $request->detail,
            'status' => $status,
            'keterangan' => $keterangan,
            'tempat' => $request->tempat,
            'catatan' => $request->catatan,
            'sumber_dana' => $request->sumber_dana,
            'anggaran' => $request->anggaran
        ];

        $data_insert_proker =[
            'id_pengajuan' => $request->id,
            'status' => 'BELUM TERLAKSANA',
        ];

        if ($level == 'pembimbing') {
            if ($keterangan == 'DITERIMA') {
                DB::table('proker')->insert($data_insert_proker);
            }
        } else if ($level == 'ketua') {
        }else{}

        DB::table('pengajuan')->where('id',$request->id)->update($data_update_pengajuan);
        return redirect('ketua/pengajuan')
        ->with('updatedpengajuan','Data Pengajuan Berhasil Terupdate!!!');
        
    }

    //membuat fungsi untuk memproses hapus
    public function hapusket($id)
    {
        Pengajuan::where('id',$id)->delete();
        return back()->with('deletedpengajuan','Data Pengajuan Berhasil Terhapus!!!');
    }

// -----------Akhir - Pengajuan - Ketua--------------------------------------------------

// pembimbing
public function indexpem()
{
//mengambil data pengajuan dengan fungsi all (semua data)

$pengajuan = Pengajuan::join('user as u','u.id','=','pengajuan.id_user')
->selectRaw("pengajuan.*, u.id as user_id,name")
->get();
// return $pengajuan;
// $pengajuan = Pengajuan::where('status','diterima')->get();

//menampilkan data ke view anggota/pengajuan 
return view('pembimbing.pengajuan', compact('pengajuan'));
}
 //fungsi untuk menampilkan form edit
 public function editpem($id)
 {
     $pengajuan = DB::table('pengajuan')->where('id',$id)->get();
     $user = DB::table('user')->selectRaw("id, name")->get();
     return view('pembimbing.editpengajuan', compact('pengajuan','user'));
 }
 //membuat fungsi untuk proses update
 public function updatepem(Request $request, Pengajuan $pengajuan)
 {

     $session = Session::get('data_user');
     $level = $session['level'];
     $status = (isset($request->status)) ? $request->status : '';
     $keterangan = (isset($request->keterangan)) ? $request->keterangan : '';

     $data_update_pengajuan = [
         'id_user' => $request->id_user,
         'nama_proker' => $request->nama_proker,
         'detail' => $request->detail,
         'status' => $status,
         'keterangan' => $keterangan,
         'tempat' => $request->tempat,
         'sumber_dana' => $request->sumber_dana,
         'anggaran' => $request->anggaran
     ];

     $data_insert_proker =[
         'id_pengajuan' => $request->id,
         'status' => 'BELUM TERLAKSANA',
     ];

     if ($level == 'pembimbing') {
         if ($keterangan == 'DITERIMA') {
             DB::table('proker')->insert($data_insert_proker);
         }
     } else if ($level == 'ketua') {
     }else{}

     DB::table('pengajuan')->where('id',$request->id)->update($data_update_pengajuan);
     return redirect('pembimbing/pengajuan')
     ->with('updatedpengajuan','Data Pengajuan Berhasil Terupdate!!!');
     
 }

}

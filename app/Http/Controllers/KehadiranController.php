<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kehadiran;
use Illuminate\Support\Facades\DB;
class KehadiranController extends Controller
{
    // ketua-----------
    public function index()
    {
        //mengambil data pengajuan dengan fungsi all (semua data)
        $kehadiran = Kehadiran::all();
       //  $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view anggota/pengajuan 
        return view('ketua.kehadiran', compact('kehadiran'));
    }

    public function tambah()
    {
       //menampilkan halaman form addpengajuan
       return view('ketua.addkehadiran');
    }

     //fungsi untuk membuat proses inputan data
   public function store(Request $request)
   {
       //validasi untuk mengisi kolom
       $this->validate($request,[
           'id_user' => 'required',
           'id_rapat' => 'required',
     
       ]);
       //fungsi untuk proses inputan data ke database
       Kehadiran::create([
           'id_user' => $request->id_user,
           'id_rapat' => $request->id_rapat,
       ]);
       return redirect('ketua/kehadiran')->with('add-kehadiran','Data kehadiran Berhasil Ditambah!!!');
   }
   public function edit($id)
   {
       $kehadiran = DB::table('kehadiran')->where('id',$id)->get();
       return view('ketua.editkehadiran', compact('kehadiran'));
   }
   //membuat fungsi untuk proses update
   public function update(Request $request, Kehadiran $kehadiran)
   {
       DB::table('kehadiran')->where('id',$request->id)->update([
           'id_user' => $request->id_user,
           'id_rapat' => $request->id_rapat
       ]);

       // rapat::find($request->id);
       return redirect('ketua/kehadiran')->with('upd-kehadiran','Data kehadiran Berhasil Diupdate!!!');
   }

    public function hapus($id)
    {
        Kehadiran::where('id',$id)->delete();
       //  return back()->with('deleted','berhasil terhapus!!!');
        return back()->with('del-kehadiran','data berhasil terhapus!!!');
    }

}

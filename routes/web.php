<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\PanitiaController;
use App\Http\Controllers\RapatController;
use App\Http\Controllers\ProkerController;
use App\Http\Controllers\LpjController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KehadiranController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//===============================ketua==================================================
//ketika membuka halaman ketua, akan muncul atau 
//mengambil view pada index.blade.php pada folder ketua
Route::view('/ketua','ketua/index');

// *********KETUA PENGAJUAN********************

// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/ketua/pengajuan', [PengajuanController::class, 'indexket']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/pengajuan/edit/{id}', [PengajuanController::class, 'editket']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/pengajuan/edit', [PengajuanController::class, 'updateket']);

// fungsi untuk memproses hapus
Route::get('/ketua/pengajuan/hapus/{id}', [PengajuanController::class, 'hapusket']);


// ###################### K E T U A P R O K E R ###################################
Route::view('/ketua/proker','ketua/proker');

// fungsi untuk menampilkan data dengan mengambil class index di prokercontroller
Route::get('/ketua/proker', [ProkerController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di prokerncontroller
Route::get('/ketua/proker/tambah', [ProkerController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di prokercontroller
Route::post('/ketua/proker/store', [ProkerController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/proker/edit/{id}', [ProkerController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/proker/edit', [ProkerController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/proker/hapus/{id}', [ProkerController::class, 'hapus']);

//export excel
Route::get('/ketua/proker/export_excel', [ProkerController::class, 'export_excel']);
//export pdf
Route::get('/ketua/proker/proker_pdf', [ProkerController::class, 'cetak_pdf']);





// ###################### K E T U A HADIR ###################################

// fungsi untuk menampilkan data dengan mengambil class index di rapatcontroller
Route::get('/ketua/kehadiran', [KehadiranController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di prokerncontroller
Route::get('/ketua/kehadiran/tambah', [KehadiranController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di KehadiranController
Route::post('/ketua/kehadiran/store', [KehadiranController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/kehadiran/edit/{id}', [KehadiranController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/kehadiran/edit', [KehadiranController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/kehadiran/hapus/{id}', [KehadiranController::class, 'hapus']);



// ###################### K E T U A R A P A T ###################################
Route::view('/ketua/rapat','ketua/rapat');

// fungsi untuk menampilkan data dengan mengambil class index di rapatcontroller
Route::get('/ketua/rapat', [RapatController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di prokerncontroller
Route::get('/ketua/rapat/tambah', [RapatController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di rapatcontroller
Route::post('/ketua/rapat/store', [RapatController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/rapat/edit/{id}', [RapatController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/rapat/edit', [RapatController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/rapat/hapus/{id_rapat}', [RapatController::class, 'hapus']);



// ##################### K E T U A P A N I T I A ###############################
Route::view('/ketua/panitia','ketua/panitia');
// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/ketua/panitia', [PanitiaController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di pengajuancontroller
Route::get('/ketua/panitia/tambah', [PanitiaController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di pengajuancontroller
Route::post('/ketua/panitia/store', [PanitiaController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/panitia/edit/{id}', [PanitiaController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/panitia/edit', [PanitiaController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/panitia/hapus/{id}', [PanitiaController::class, 'hapus']);

Route::get('/ketua/panitia/dataTable', [PanitiaController::class, 'dataTable']);
// ***************************************************************************************

// KELOLA USER
Route::get('/ketua/user', [UserController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di pengajuancontroller
Route::get('/ketua/user/tambah', [UserController::class, 'tambah']);



// fungsi untuk memproses tambah data dengan mengambil class store di pengajuancontroller
Route::post('/ketua/user/store', [UserController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/user/edit/{id}', [UserController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/user/edit', [UserController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/user/hapus/{id}', [UserController::class, 'hapus']);



// ###################### K E T U A A N G G O T A ###################################
Route::view('/ketua/anggota','ketua/anggota');

// ###################### K E T U A S T R U K T U R ###################################
Route::view('/ketua/struktur','ketua/struktur');

// ###################### K E T U A P E N D A F T A R ###################################
Route::view('/ketua/pendaftar','ketua/pendaftar');






//===========================ANGGOTA=======================================================
// DASHBOARD
Route::view('/anggota','anggota/index');

// *********PENGAJUAN********************
Route::view('/anggota/pengajuan','anggota/pengajuan');

// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/anggota/pengajuan', [PengajuanController::class, 'index']);
// // fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
// // fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/anggota/pengajuan', [PengajuanController::class, 'index']);


Route::get('/anggota/lpj', [LpjController::class, 'index']);
// Route::get('/anggota/addlpj/upload/{id}', [LpjController::class, 'edit']);
Route::get('/anggota/addlpj/upload', [LpjController::class, 'upload']);
Route::post('/anggota/lpj/proses_upload',[LpjController::class,'proses_upload']);



// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di pengajuancontroller
Route::get('/anggota/pengajuan/tambah', [PengajuanController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di pengajuancontroller
Route::post('/anggota/pengajuan/store', [PengajuanController::class, 'store']);


// ******************************AKHIR PENGAJUAN*************************************
// ******************Anggota Proker******************** 
Route::view('/anggota/proker','anggota/proker');

// fungsi untuk menampilkan data dengan mengambil class index di prokercontroller
Route::get('/anggota/proker', [ProkerController::class, 'indexanggota']);
Route::get('/anggota/panitia', [PanitiaController::class, 'indexanggota']);
Route::get('/anggota/rapat', [RapatController::class, 'indexanggota']);
Route::view('/coba','anggota.template.v_template');

// ***************************************************************************************


//==========================PEMBIMBING==================================================
Route::view('/pembimbing','pembimbing/index');
// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/pembimbing/pengajuan', [PengajuanController::class, 'indexpem']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/pembimbing/pengajuan/edit/{id}', [PengajuanController::class, 'editpem']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/pembimbing/pengajuan/edit', [PengajuanController::class, 'updatepem']);

// fungsi untuk menampilkan data dengan mengambil class index di prokercontroller
Route::get('/pembimbing/proker', [ProkerController::class, 'indexpem']);

Auth::routes();
Route::post('/kirim_login', [LoginController::class, 'proses_login'])->name('kirim_login');
Route::get('/upd_pass', [LoginController::class, 'upd_pass'])->name('upd_pass');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

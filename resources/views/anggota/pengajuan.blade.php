@extends('anggota.template.v_template')
@section('title','PENGAJUAN')

@section('content')
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah pengajuan program kerja. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL PENGAJUAN --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Pengajuan Program Kerja</h3>
      </div>
  </div>
  <hr>

  <div class="container" style="margin-left: -15px;">
    <a href="/anggota/pengajuan/tambah" class="btn btn-warning">+ Tambah Pengajuan</a>
 </div>
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif
<div class="table-responsive">


      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
              <th style="color: white">PROGRAM KERJA</th>
              <th style="color: white">DETAIL PROKER</th>
              <th style="color: white">TEMPAT</th>
              <th style="color: white">SUMBER DANA</th>
              <th style="color: white">ANGGARAN</th>
              <th style="color: white">STATUS</th>
              <th style="color: white">KETERANGAN</th>
              <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
          @foreach($pengajuan as $p)
          <tr>
                <td>{{$p->nama_proker}}</td>
                  <td>{{$p->detail}}</td>
                  <td>{{$p->tempat}}</td>
                  <td>{{$p->sumber_dana}}</td>
                  <td>{{$p->anggaran}}</td>
                  <td>{{$p->status}}</td>
                  <td>{{$p->keterangan}}</td>
                  
              
                <td>
                    <a href="#" class="btn btn-success btn-sm btnDetail"
                    data-idpengajuan="{{$p->id}}"
                    data-iduser="{{$p->id_user}}"
                    data-nama="{{$p->name}}"
                    data-proker="{{$p->nama_proker}}"
                    data-detail="{{$p->detail}}"
                    data-tempat="{{$p->tempat}}"
                    data-sumber_dana="{{$p->sumber_dana}}"
                    data-anggaran="{{$p->anggaran}}"
                    data-status="{{$p->status}}"
                    data-keterangan="{{$p->keterangan}}"
                    data-catatan="{{$p->catatan}}"
                    >
                        <i class="fa fa-eye"></i> Detail Data
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
</div>
{{-- Modal --}}

<div class="modal modal-info fade" id="modal_rapat">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <div class="col-lg-9 margin-tb">
            <h3 class="pl-1" style="border-left: solid rgb(177, 5, 154) 5px">&nbsp;Detail Pengajuan Program Kerja</h3>
        </div>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <p>ID PENGAJUAN</p>
                    <input type="text" id="idpengajuan1">

                    <p>ID USER</p>
                    <p id="tgl_detail"></p>
                    <input type="text" id="iduser1">

                    <p>NAMA</p>
                    <input type="text" id="nama1">

                    <p>PROGRAM KERJA</p>
                    <input type="text" id="proker1">

                    <p>DETAIL PROKER</p>
                    <input type="text" id="detailproker1">
                    

                    
                </div>

                <div class="col-md-6">
                    
                    <p>TEMPAT PELAKSANAAN</p>
                    <input type="text" id="tempat1">

                    <p>SUMBER DANA</p>
                    <input type="text" id="sumber_dana1">

                    <p>ANGGARAN</p>
                    <input type="text" id="anggaran1">

                    <p>STATUS</p>
                    <input type="text" id="status1">

                    <p>KETERANGAN</p>
                    <input type="text" id="keterangan1">

                </div>

                <div class="col-md-12">
                    
                    <p>CATATAN</p>
                    <input type="text" id="catatan1">
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-outline">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
{{-- Akhir Modal --}}
<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idpengajuan1').val($(this).data('idpengajuan'));
        $('#iduser1').val($(this).data('iduser'));
        $('#nama1').val($(this).data('nama'));
        $('#proker1').val($(this).data('proker'));
        $('#detailproker1').val($(this).data('detail'));
        $('#tempat1').val($(this).data('tempat'));
        $('#sumber_dana1').val($(this).data('sumber_dana'));
        $('#anggaran1').val($(this).data('anggaran'));
        $('#status1').val($(this).data('status'));
        $('#keterangan1').val($(this).data('keterangan'));
        $('#catatan1').val($(this).data('catatan'));

        $('#modal_rapat').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

{{-- js --}}
{{-- @if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif --}}
<script>
  
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

{{-- AKHIR PENGAJUAN --}}

{{-- jquery cdn --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{-- toast --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>

@if(Session::has('pengangg1'))
    <script>
        toastr.success("{!! Session::get('pengangg1') !!}");
    </script>
@endif

{{-- sweet alert --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>

@if(Session::has('pengangg1'))
    <script>
        swal("Great Job!","{!! Session::get('pengangg1') !!}","success",{
            button:"OK",
        });
    </script>
@endif

@endsection

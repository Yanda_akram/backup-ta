  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{ request()->is('anggota') ? 'active' : ''}}"><a href="/anggota"> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
    <li class="{{ request()->is('anggota/pengajuan') ? 'active' : ''}}"><a href="/anggota/pengajuan"> <i class="fa fa-file"></i> <span>PENGAJUAN</span></a></li>
    <li class="treeview">
      <a href="/anggota">
        <i class="fa fa-dashboard"></i> <span>PROGRAM KERJA</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="{{ request()->is('anggota/proker') ? 'active' : ''}}"><a href="/anggota/proker"><i class="fa fa-circle-o"></i> Program kerja</a></li>
        <li class="{{ request()->is('anggota/lpj') ? 'active' : ''}}"><a href="/anggota/lpj"><i class="fa fa-circle-o"></i>Dokumen LPJ</a></li>
      </ul>
    </li>
   
    <li class="{{ request()->is('anggota/panitia') ? 'active' : ''}}"><a href="/anggota/panitia"><i class="fa fa-users"></i> <span>PANITIA</span></a></li>
    
    <li class="{{ request()->is('anggota/rapat') ? 'active' : ''}}"><a href="/anggota/rapat"><i class="fa fa-book"></i> <span>RAPAT</span></a></li>
    <li class="{{ request()->is('anggota/kehadiran') ? 'active' : ''}}"><a href="/anggota/kehadiran"><i class="fa fa-book"></i> <span>REKAP KEHADIRAN</span></a></li>
    <li class="header">LABELS</li>

  </ul>
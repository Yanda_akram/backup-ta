@extends('anggota.template.v_template')
@section('title','Rapat')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

</head>
<body>
      
<br>

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah pengajuan program kerja. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL PANITIA --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Notula Rapat Program Kerja</h3>
      </div>
    </div>
  <hr>

 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

 <div class="table-responsive">

 
      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
            < <th style="color: white">TANGGAL</th>
            <th style="color: white">TEMPAT</th>
            <th style="color: white">TEMA</th>
          
            <th style="color: white">JUMLAH PESERTA</th>
            <th style="color: white">PENYAJI</th>
            <th style="color: white">SUSUNAN ACARA</th>
            <th style="color: white">KESIMPULAN</th>
          
            <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
          @foreach($rapat as $p)
          <tr>
            <td>{{$p->tanggal}}</td>
            <td>{{$p->tempat}}</td>
            <td>{{$p->tema}}</td>
            <td>{{$p->jumlah_peserta}}</td>
            <td>{{$p->penyaji}}</td>
            <td>{{$p->susunan_acara}}</td>
            <td>{{$p->catatan}}</td>
              
                <td> 
                    <a href="#" class="btn btn-info btn-sm btnDetail"
                    data-idrapat="{{$p->id}}"
                    data-idproker="{{$p->id_proker}}"
                    data-tanggal="{{$p->tanggal}}"
                    data-tempat="{{$p->tempat}}"
                    data-tema="{{$p->tema}}"

                  
                    data-jumlahpeserta="{{$p->jumlah_peserta}}"
                    data-penyaji="{{$p->penyaji}}"
                    data-susunanacara="{{$p->susunan_acara}}"
                    data-catatan="{{$p->catatan}}"
                    >
                        <i class="fa fa-eye"></i> Detail
                    </a>
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
</div>
{{-- js --}}
{{-- Modal --}}

    <div class="modal modal-info fade" id="modal_rapat">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <div class="col-lg-9 margin-tb">
                <h3 class="pl-1" style="border-left: solid white 5px">&nbsp;Detail Notula Rapat Program Kerja</h3>
            </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <p>ID RAPAT</p>
                        <input type="text" id="idrapat1">

                        <p>TANGGAL</p>
                        <p id="tgl_detail"></p>
                        <input type="date" id="tgl_detail1">

                        <p>TEMA</p>
                        <input type="text" id="tema1">

                        <p>JUMLAH PESERTA</p>
                        <input type="text" id="jumlahpeserta1">

                        <p>PENYAJI</p>
                        <input type="text" id="penyaji1">

                        

                    </div>
                    <div class="col-md-6">
                        <p>ID PROKER</p>
                        <input type="text" id="idproker1">

                        <p>TEMPAT</p>
                        <input type="text" id="tempat1">
                        
                        
                        <p>SUSUNAN ACARA</p>
                        <input type="text" id="susunanacara1">
                        
                        <p>CATATAN</p>
                        <input type="text" id="catatan1">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
{{-- Akhir Modal --}}

<script>
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}

{{-- AKHIR RAPAT --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

@if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif

<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idrapat1').val($(this).data('idrapat'));
        $('#idproker1').val($(this).data('idproker'));
        $('#tgl_detail1').val($(this).data('tanggal'));
        $('#tempat1').val($(this).data('tempat'));
        $('#tema1').val($(this).data('tema'));
      
        $('#jumlahpeserta1').val($(this).data('jumlahpeserta'));
        $('#penyaji1').val($(this).data('penyaji'));
       
        $('#susunanacara1').val($(this).data('susunanacara'));
        $('#catatan1').val($(this).data('catatan'));


        $('#modal_rapat').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

</body>
</html>
@endsection

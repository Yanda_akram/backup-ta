@extends('anggota.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Pengajuan Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data pengajuan program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/anggota/pengajuan/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
           
             
             <div class="form-group">
                 <label>ID USER</label>
                 <select class="user12" name="id_user" class="form-control">
                     @forelse ($user as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @empty
                        <option value="0">data kosong</option>
                     @endforelse
                 </select>
                </div>
                
            @if($errors->has('id_user'))
                    <div class="text-danger">
                        {{ $errors->first('id_user')}}
                    </div>
            @endif

            <div class="form-group">
                <label>PROGRAM KERJA</label>
                <input type="text" name="nama_proker" class="form-control">
            </div>
            @if($errors->has('nama_proker'))
                    <div class="text-danger">
                        {{ $errors->first('nama_proker')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label>DETAIL PROKER</label>
                <input type="text" name="detail" class="form-control">
            </div>
            @if($errors->has('detail'))
                    <div class="text-danger">
                        {{ $errors->first('detail')}}
                    </div>
            @endif

        </div>
        
            
        <div class="col-sm-6">
            <div class="form-group">
                <label>TEMPAT</label>
                <input type="text" name="tempat" class="form-control">
            </div>
            @if($errors->has('tempat'))
                    <div class="text-danger">
                        {{ $errors->first('tempat')}}
                    </div>
            @endif 

            <div class="form-group">
                <label>SUMBER DANA</label>
                <select class="form-control" name="sumber_dana">
                    <option selected disabled> -- PILIH SUMBER DANA --
                    </option>
                    <option value="KAS BEI"> KAS BEI
                    </option>
                    <option value="SPONSORSHIP"> SPONSORSHIP
                    </option>
                    <option value="KAMPUS"> KAMPUS
                    </option>
                    <option value="LAINNYA"> LAINNYA
                    </option>
                </select>
            </div>
            @if($errors->has('sumber_dana'))
            <div class="text-danger">
                {{ $errors->first('sumber_dana')}}
            </div>
    @endif
            
            <div class="form-group">
                <label>ANGGARAN</label>
                <input type="text" name="anggaran" class="form-control">
            </div>
            @if($errors->has('anggaran'))
                    <div class="text-danger">
                        {{ $errors->first('anggaran')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label></label>
                <input type="hidden" name="status" value="DIPROSES" class="form-control">
            </div> 
            <div class="form-group">
                <label></label>
                <input type="hidden" name="catatan" value="-" class="form-control">
            </div> 
            
            <div class="form-group">
                <label></label>
                <input type="hidden" name="keterangan" value="DIPROSES" class="form-control">
            </div> 
        </div>
        
        <div class="form-group">
            <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
            </div>
            
        </div>
    </div>

</form>

<script>
    $(document).ready(function() {
        $(".user12").select2({
            width: '100%'
        });
        
    });
</script>

@endsection
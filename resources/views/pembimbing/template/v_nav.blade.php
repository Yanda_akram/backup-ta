  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{ request()->is('pembimbing') ? 'active' : ''}}"><a href="/pembimbing"> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
    <li class="{{ request()->is('pembimbing/pengajuan') ? 'active' : ''}}"><a href="/pembimbing/pengajuan"> <i class="fa fa-file"></i> <span>PENGAJUAN</span></a></li>
    <li class="treeview">
      <a href="/pembimbing">
        <i class="fa fa-dashboard"></i> <span>PROGRAM KERJA</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="{{ request()->is('pembimbing/proker') ? 'active' : ''}}"><a href="/pembimbing/proker"><i class="fa fa-circle-o"></i> Program kerja</a></li>
        <li class="{{ request()->is('pembimbing/lpj') ? 'active' : ''}}"><a href="/pembimbing/lpj"><i class="fa fa-circle-o"></i>Dokumen LPJ</a></li>
      </ul>
    </li>
    <li class="header">LABELS</li>

  </ul>
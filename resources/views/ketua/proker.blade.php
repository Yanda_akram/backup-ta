@extends('ketua.template.v_template')
@section('title','Proker')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

</head>
<body>
      
<br>

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat mengelola program kerja. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL PANITIA --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Proker Program Kerja</h3>
      </div>
  </div>
  <hr>

<div class="container" style="margin-left: -15px;">
    {{-- <a href="/ketua/proker/tambah" class="btn btn-warning">+ Tambah Proker</a> --}}
    <a href="/ketua/proker/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
    <a href="/ketua/proker/proker_pdf" class="btn btn-danger my-3" target="_blank">EXPORT PDF</a>
 </div>
		
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

 <div class="table-responsive">
      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
              <th style="color: white">NAMA PROKER</th>
              <th style="color: white">TANGGAL</th>
              <th style="color: white">TEMPAT PELAKSANAAN</th>
              <th style="color: white">ANGGARAN</th>
              <th style="color: white">STATUS</th>
              <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
          @foreach($proker as $p)
          <tr>
                  <td>{{$p->nama_proker}}</td>
                  <td>{{$p->tanggal}}</td>
                  <td>{{$p->tempat}}</td>
                  <td>{{$p->anggaran}}</td>
                  <td>{{$p->status}}</td>
                <td> 
                    <a href="/ketua/proker/edit/{{ $p->id }}" class="btn btn-info btn-sm"><i class="fa fa-check"></i> Edit Data</a> <br>
                    <a href="/ketua/proker/hapus/{{ $p->id }}" onclick="return confirm('Apakah anda yakin untuk menghapus?')"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Data</a> <br>
                    <a href="#" class="btn btn-success btn-sm btnDetail"
                    data-idproker="{{$p->id}}"
                    data-idpengajuan="{{$p->id_pengajuan}}"
                    data-namaproker="{{$p->nama_proker}}"
                    data-detail="{{$p->detail}}"
                    data-tanggal="{{$p->tanggal}}"
                    data-tempat="{{$p->tempat}}"
                    data-anggaran="{{$p->anggaran}}"
                    data-status="{{$p->status}}"
                    >
                        <i class="fa fa-eye"></i> Detail Data
                    </a>
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
</div>
{{-- js --}}

{{-- Modal --}}

<div class="modal modal-info fade" id="modal_rapat">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <div class="col-lg-9 margin-tb">
            <h3 class="pl-1" style="border-left: solid rgb(177, 5, 154) 5px">&nbsp;Detail Pengajuan Program Kerja</h3>
        </div>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <p>ID PROKER</p>
                    <input type="text" id="idproker1">

                    <p>ID PENGAJUAN</p>
                    {{-- <p id="tgl_detail"></p> --}}
                    <input type="text" id="idpengajuan1">

                    <p>NAMA PROKER</p>
                    <input type="text" id="namaproker1">

                    <p>DETAIL PROKER</p>
                    <input type="text" id="detail1">
                </div>

                <div class="col-md-6">
                    <p>TANGGAL PELAKSANAAN</p>
                    <input type="date" id="tanggal1">

                    <p>TEMPAT PELAKSANAAN</p>
                    <input type="text" id="tempat1">
                    
                    
                    <p>ANGGARAN</p>
                    <input type="text" id="anggaran1">
                    
                    
                    
                    <p>STATUS</p>
                    <input type="text" id="status1">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-outline">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
{{-- Akhir Modal --}}

<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idproker1').val($(this).data('idproker'));
        $('#idpengajuan1').val($(this).data('idpengajuan'));
        $('#namaproker1').val($(this).data('namaproker'));
        $('#detail1').val($(this).data('detail'));
        $('#tanggal1').val($(this).data('tanggal'));
        $('#tempat1').val($(this).data('tempat'));
        $('#anggaran1').val($(this).data('anggaran'));
        $('#status1').val($(this).data('status'));

        $('#modal_rapat').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

{{-- jquery cdn --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{-- toast --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>

@if(Session::has('deletedproker'))
    <script>
        toastr.success("{!! Session::get('deletedproker') !!}");
    </script>
@endif
@if(Session::has('updatedproker'))
    <script>
        toastr.success("{!! Session::get('updatedproker') !!}");
    </script>
@endif
@if(Session::has('createdproker'))
    <script>
        toastr.success("{!! Session::get('createdproker') !!}");
    </script>
@endif


{{-- sweet alert --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>

@if(Session::has('deletedproker'))
    <script>
        swal("Great Job!","{!! Session::get('deletedproker') !!}","success",{
            button:"OK",
        });
    </script>
@endif
@if(Session::has('createdproker'))
    <script>
        swal("Great Job!","{!! Session::get('createdproker') !!}","success",{
            button:"OK",
        });
    </script>
@endif
@if(Session::has('updatedproker'))
    <script>
        swal("Great Job!","{!! Session::get('updatedproker') !!}","success",{
            button:"OK",
        });
    </script>
@endif


<script>
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}

{{-- AKHIR PANITIA --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

@if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif
  

</body>
</html>
@endsection

@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form proker Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data proker program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/rapat/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        
        <div class="row">   
            <div class="col-sm-6">
            <div class="form-group">
                <label>ID PROKER</label>
                <input type="text" name="id_proker" class="form-control">
            </div>
            @if($errors->has('id_proker'))
                <div class="text-danger">
                    {{ $errors->first('id_proker')}}
                </div>
             @endif
             
            <div class="form-group">
                <label>TANGGAL</label>
                <input type="date" name="tanggal" class="form-control">
            </div>
            @if($errors->has('tanggal'))
                    <div class="text-danger">
                        {{ $errors->first('tanggal')}}
                    </div>
            @endif
            <div class="form-group">
                <label>TEMPAT</label>
                <input type="text" name="tempat" class="form-control">
            </div>
            @if($errors->has('tempat'))
                <div class="text-danger">
                    {{ $errors->first('tempat')}}
                </div>
             @endif
             
      
            <div class="form-group">
                <label>TEMA</label>
                <input type="text" name="tema" class="form-control">
            </div>
            @if($errors->has('tema'))
                    <div class="text-danger">
                        {{ $errors->first('tema')}}
                    </div>
            @endif
            

        </div>
        
            
        <div class="col-sm-6">

        
            <div class="form-group">
                <label>JUMLAH PESERTA</label>
                <input type="text" name="jumlah_peserta" class="form-control">
            </div>
            @if($errors->has('jumlah_peserta'))
                    <div class="text-danger">
                        {{ $errors->first('jumlah_peserta')}}
                    </div>
            @endif
            
           
            <div class="form-group">
                <label>PENYAJI</label>
                <input type="text" name="penyaji" class="form-control">
            </div>
            @if($errors->has('penyaji'))
                    <div class="text-danger">
                        {{ $errors->first('penyaji')}}
                    </div>
            @endif
            <div class="form-group">
                <label>SUSUNAN ACARA</label>
                <input type="text" name="susunan_acara" class="form-control">
            </div>
            @if($errors->has('susunan_acara'))
                    <div class="text-danger">
                        {{ $errors->first('susunan_acara')}}
                    </div>
            @endif
          
            <div class="form-group">
                <label>CATATAN</label>
                <input type="text" name="catatan" class="form-control">
            </div>
            @if($errors->has('catatan'))
                    <div class="text-danger">
                        {{ $errors->first('catatan')}}
                    </div>
            @endif
        </div>
        
        @if(Session::has(''))
            <script>
                toasts.success("{!! Session::get('') !!}");
            </script>
        @endif
            
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
        </div>
</form>


<script>
    $(document).ready(function() {
        $(".pengajuan").select2({
            width: '100%'
        });
        
    });
</script>
@endsection
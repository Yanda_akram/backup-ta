@extends('ketua.template.v_template')
@section('title','Proker')
@section('content')
    
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah pengajuan program kerja. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL PANITIA --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Panitia Program Kerja</h3>
      </div>
  </div>
  <hr>

<div class="container" style="margin-left: -15px;">
    <a href="/ketua/panitia/tambah" class="btn btn-warning">+ Tambah Panitia</a>
 </div>
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
              <th style="color: white">PROKER</th>
              <th style="color: white">NAMA USER</th>
              <th style="color: white">JABATAN KEPANITIAAN</th>
              <th style="color: white">DIVISI</th>
              <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
          @foreach($panitia as $p)
          <tr>
              <td>{{$p->nama_proker}}</td>
                  <td>{{$p->name}}</td>
                  <td>{{$p->jabatan}}</td>              
                  <td>{{$p->divisi}}</td>              
                <td> 
                    <a href="/ketua/panitia/edit/{{ $p->id }}" class="btn btn-info btn-sm"><i class="fa fa-check"></i> Edit Data</a> 
                    <a href="/ketua/panitia/hapus/{{ $p->id }}" onclick="return confirm('Apakah anda yakin untuk menghapus?')"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Data</a> 
                    <a href="#" class="btn btn-success btn-sm btnDetail"
                    data-idpanitia="{{$p->id}}"
                    data-name="{{$p->name}}"
                    data-nama_proker="{{$p->nama_proker}}"
                    data-jabatan="{{$p->jabatan}}"
                    data-divisi="{{$p->divisi}}"
                    >
                        <i class="fa fa-eye"></i> Detail Data
                    </a>
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
{{-- js --}}
{{-- Modal --}}

<div class="modal modal-info fade" id="modal_rapat">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <div class="col-lg-9 margin-tb">
            <h3 class="pl-1" style="border-left: solid rgb(177, 5, 154) 5px">&nbsp;Detail Panitia Program Kerja</h3>
        </div>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <p>ID PANITIA</p>
                    <input type="text" id="idpanitia1">

                    <p>NAMA PANITIA</p>
                    {{-- <p id="tgl_detail"></p> --}}
                    <input type="text" id="nama1">

                    <p>PANITIA PROKER</p>
                    <input type="text" id="proker1">

                    <p>JABATAN PANITIA</p>
                    <input type="text" id="jabatan1">

                    <p>DIVISI PANITIA</p>
                    <input type="text" id="divisi1">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-outline pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
{{-- Akhir Modal --}}

<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idpanitia1').val($(this).data('idpanitia'));
        $('#nama1').val($(this).data('name'));
        $('#proker1').val($(this).data('nama_proker'));
        $('#jabatan1').val($(this).data('jabatan'));
        $('#divisi1').val($(this).data('divisi'));
        $('#modal_rapat').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

{{-- @if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif --}}
<script>
  
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>


{{-- jquery cdn --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{-- toast --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>

@if(Session::has('deletedpanitia'))
    <script>
        toastr.success("{!! Session::get('deletedpanitia') !!}");
    </script>
@endif
@if(Session::has('updatedpanitia'))
    <script>
        toastr.success("{!! Session::get('updatedpanitia') !!}");
    </script>
@endif
@if(Session::has('createdpanitia'))
    <script>
        toastr.success("{!! Session::get('createdpanitia') !!}");
    </script>
@endif


{{-- sweet alert --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>

@if(Session::has('deletedpanitia'))
    <script>
        swal("Great Job!","{!! Session::get('deletedpanitia') !!}","success",{
            button:"OK",
        });
    </script>
@endif
@if(Session::has('createdpanitia'))
    <script>
        swal("Great Job!","{!! Session::get('createdpanitia') !!}","success",{
            button:"OK",
        });
    </script>
@endif
@if(Session::has('updatedpanitia'))
    <script>
        swal("Great Job!","{!! Session::get('updatedpanitia') !!}","success",{
            button:"OK",
        });
    </script>
@endif

{{-- js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

{{-- AKHIR PANITIA --}}
@endsection

@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE KEHADIRAN</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($kehadiran as $p)
    <form class="col-md-12" action="/ketua/kehadiran/edit" method="post">
    {{ csrf_field() }}
    <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
      
      <div class="row">
        <div class="form-group col-sm-12">
          <label for="id_user" class="form-label"><b>ID PROKER</b></label>
          <input type="text" class="form-control" name="id_user" value="{{ $p->id_user }}" >

          <label for="id_rapat" class="form-label"><b>ID RAPAT</b></label>
          <input type="text" class="form-control" name="id_rapat" value="{{ $p->id_rapat }}" >

        </div>
      
      
    </div>
    <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
    <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
      
    </form>
    @endforeach
    
    {{-- <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script> --}}
@endsection
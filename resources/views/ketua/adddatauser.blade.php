@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Pengguna ASLAB GI-BEI</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data pengguna aplikasi ASLAB GI-BEI</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/user/store" method="POST" enctype="multipart/form-data">

    @csrf
    <table class="table" id="table-adduser">
        <thead class="table" style="background-color: #d16908"  >
            <tr>
            <th style="color: white">NAMA PENGGUNA</th>
            <th style="color: white">USERNAME</th>
            <th style="color: white">PASSWORD</th>
            <th style="color: white">LEVEL</th>
            <th style="color: white">AKSI</th>
        </tr>
    </thead>
    <tbody id="user_formtambah">
        <tr id="userRow0">
            <td><input type="text" name="name[]" class="form-control"></td>
            <td><input type="text" name="username[]" class="form-control"></td>
            <td><input type="text" name="password[]" class="form-control"> </td>
            <td>  <input type="text" name="level[]" class="form-control">
             </td>
             <td><button class="btn btn-danger btn-sm rmRow" data-id="0"><i class="fa fa-trash"></i></button> </td>
        </tr>
    </tbody>    
</table>
<div class="form-group">
    <button class="btn btn-warning btn-sm" id="btn_addForm" style="margin-left: 8pt;"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button> 
    <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;SIMPAN</button>
    </div>
</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    var count = 1;

    $(document).ready(function() {
        $(".user2").select2({
            width: '100%'
        });
        $(".proker2").select2({
            width: '100%'
        });
    });

    $('#btn_addForm').click(function (e) { 
        e.preventDefault();
        var content =  '<tr id="userRow'+count+'"><td><input type="text" name="name[]" class="form-control">';
           
            content += '</td><td><input type="text" name="username[]" class="form-control"></td>';
           
            content += '<td><input type="text" name="password[]" class="form-control"></td>';

            content += '<td><input type="text" name="level[]" class="form-control"></td>';
            content += '<td><button class="btn btn-danger btn-sm rmRow" data-id="'+count+'"><i class="fa fa-trash"></i></button></td></tr>';
        $('#user_formtambah').append(content);
        
        count++;
    });

    $(document).on('click','.rmRow', function (e) { 
        e.preventDefault();
        var id = $(this).data('id');
        // console.log(id);
        $('#userRow'+id).remove();
        
    });
</script>

@endsection
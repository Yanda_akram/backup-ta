<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th style="color: black">NAMA PROKER</th>
            <th style="color: black">TANGGAL</th>
            <th style="color: black">TEMPAT</th>
            <th style="color: black">ANGGARAN</th>
            <th style="color: black">STATUS</th>
        </tr>
        </thead>
        <tbody>
        @foreach($proker as $p)
            <tr>
                <td>{{$p->nama_proker}}</td>
                <td>{{$p->tanggal}}</td>
                <td>{{$p->tempat}}</td>
                <td>Rp. {{$p->anggaran}}</td>
                <td>{{$p->status}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form proker Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data proker program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/proker/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
            <div class="form-group">
                <label>NAMA PROKER</label>
                <select class="pengajuan" name="id_pengajuan" class="form-control">
                    @forelse ($pengajuan as $item)
                       <option value="{{$item->id}}">{{$item->nama_proker}}</option>
                       @empty
                       <option value="0">data kosong</option>
                    @endforelse
                </select>
            </div>
            @if($errors->has('id_pengajuan'))
                <div class="text-danger">
                    {{ $errors->first('id_pengajuan')}}
                </div>
             @endif
             
      
            <div class="form-group">
                <label>ANGGARAN</label>
                <input type="text" name="anggaran" class="form-control">
            </div>
            @if($errors->has('anggaran'))
                    <div class="text-danger">
                        {{ $errors->first('anggaran')}}
                    </div>
            @endif
            
          

        </div>
        
            
        <div class="col-sm-6">

        
            <div class="form-group">
                <label>TANGGAL</label>
                <input type="date" name="tanggal" class="form-control">
            </div>
            @if($errors->has('tanggal'))
                    <div class="text-danger">
                        {{ $errors->first('tanggal')}}
                    </div>
            @endif
            
           
            <div class="form-group">
                <label>STATUS PROKER</label>
                <select class="form-control" name="status">
                    <option value="TERLAKSANA"> TERLAKSANA
                    </option>
                    <option value="BELUM TERLAKSANA"> BELUM TERLAKSANA
                    </option>
                </select>
            </div>
            @if($errors->has('status'))
                    <div class="text-danger">
                        {{ $errors->first('status')}}
                    </div>
            @endif
            

           
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>TEMPAT</label>
                <input type="text" name="tempat" class="form-control">
            </div>
            @if($errors->has('tempat'))
                    <div class="text-danger">
                        {{ $errors->first('tempat')}}
                    </div>
            @endif
        </div>
        
        @if(Session::has(''))
            <script>
                toasts.success("{!! Session::get('') !!}");
            </script>
        @endif
            
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
        </div>
</form>


<script>
    $(document).ready(function() {
        $(".pengajuan").select2({
            width: '100%'
        });
        
    });
</script>
@endsection
@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE RAPAT</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($rapat as $p)
    <form class="col-md-12" action="/ketua/rapat/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_proker" class="form-label"><b>ID PROKER</b></label>
          <input type="text" class="form-control" name="id_proker" value="{{ $p->id_proker }}" >

          <label for="tanggal" class="form-label"><b>TANGGAL</b></label>
          <input type="text" class="form-control" name="tanggal" value="{{ $p->tanggal }}" >

          <label for="tempat" class="form-label"><b>TEMPAT</b></label>
          <input type="text" class="form-control" name="tempat" value="{{ $p->tempat }}" >
          
          <label for="jumlah_peserta" class="form-label"><b>JUMLAH PESERTA</b></label>
          <input type="text" class="form-control" name="jumlah_peserta" value="{{ $p->jumlah_peserta }}" >

         
       
        </div>
      
        <div class="form-group col-sm-6">
          <label for="tema" class="form-label"><b>TEMA</b></label>
          <input type="text" class="form-control" name="tema" value="{{ $p->tema }}" >

          
          <label for="susunan_acara" class="form-label"><b>SUSUNAN ACARA</b></label>
          <input type="text" class="form-control" name="susunan_acara" value="{{ $p->susunan_acara }}" >

          <label for="catatan" class="form-label"><b>CATATAN</b></label>
          <input type="text" class="form-control" name="catatan" value="{{ $p->catatan }}" >  
          
          <label for="penyaji" class="form-label"><b>PENYAJI</b></label>
          <input type="text" class="form-control" name="penyaji" value="{{ $p->penyaji }}" >
        
      </div>
      
    </div>
    <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
    <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
      
    </form>
    @endforeach
    
    {{-- <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script> --}}
@endsection
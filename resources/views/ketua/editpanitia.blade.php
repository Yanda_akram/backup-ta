@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE PANITIA</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($panitia as $p)
    <form class="col-md-12" action="/ketua/panitia/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_user" class="form-label"><b>ID USER</b></label>
          {{-- <input type="text" class="form-control" name="id_user" value="{{ $p->id_user }}" > --}}
          <select class="user0" name="id_user" class="form-control">
            @forelse ($user as $item)
               <option value="{{$item->id}}" {{ ($item->id == $p->id_user)? 'selected' : ''}}>{{$item->name}}</option>
               @empty
               <option value="0">data kosong</option>
            @endforelse
        </select>
        

          <label for="id_proker" class="form-label"><b>ID PROKER</b></label>
        
                <select class="pengajuan7" name="id_proker" class="form-control">
            @forelse ($pengajuan as $item)
               <option value="{{$item->id}}" {{ ($item->id == $p->id)? 'selected' : ''}}>{{$item->nama_proker}}</option>
               @empty
               <option value="0">data kosong</option>
            @endforelse
        </select>

          
      </div>
      <div class="form-group col-sm-6">
        <label for="jabatan" class="form-label"><b>JABATAN KEPANITIAAN</b></label>
        {{-- <input type="text" class="form-control" name="jabatan" value="{{ $p->jabatan }}" > --}}
        <select class="form-control" name="jabatan">
          <option selected disabled> -- PILIH JABATAN --
          </option>
          <option value="KOORDINATOR"> KOORDINATOR
          </option>
          <option value="ANGGOTA"> ANGGOTA
          </option>
      </select>

        <label for="divisi" class="form-label"><b>divisi</b></label>
        <input type="text" class="form-control" name="divisi" value="{{ $p->divisi }}" >
        </div>
        
        
        
      </div>
      <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
      <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".user0").select2({
              width: '100%'
          });
          
      });
  </script>
    <script>
      $(document).ready(function() {
          $(".pengajuan7").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">PROKER ASLAB</li>
    <li class="{{ request()->is('ketua') ? 'active' : ''}}"><a href="/ketua"> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
    <li class="{{ request()->is('ketua/pengajuan') ? 'active' : ''}}"><a href="/ketua/pengajuan"> <i class="fa fa-file"></i> <span>PENGAJUAN</span></a></li>
    <li class="treeview">
      <a href="/ketua">
        <i class="fa fa-dashboard"></i> <span>PROGRAM KERJA</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="{{ request()->is('ketua/proker') ? 'active' : ''}}"><a href="/ketua/proker"><i class="fa fa-circle-o"></i> Program kerja</a></li>
        <li class="{{ request()->is('ketua/lpj') ? 'active' : ''}}"><a href="/ketua/lpj"><i class="fa fa-circle-o"></i>Dokumen LPJ</a></li>
      </ul>
    </li>
   
    <li class="{{ request()->is('ketua/panitia') ? 'active' : ''}}"><a href="/ketua/panitia"><i class="fa fa-users"></i> <span>PANITIA</span></a></li>
    
    <li class="{{ request()->is('ketua/rapat') ? 'active' : ''}}"><a href="/ketua/rapat"><i class="fa fa-book"></i> <span>RAPAT</span></a></li>
    <li class="{{ request()->is('ketua/kehadiran') ? 'active' : ''}}"><a href="/ketua/kehadiran"><i class="fa fa-book"></i> <span>REKAP KEHADIRAN</span></a></li>

    {{-- KEANGGOTAAN --}}
    <li class="header">KEANGGOTAAN</li>
    <li class="{{ request()->is('ketua/anggota') ? 'active' : ''}}"><a href="/ketua/anggota"> <i class="fa fa-file"></i> <span>KELOLA ANGGOTA</span></a></li>
   
    <li class="{{ request()->is('ketua/struktur') ? 'active' : ''}}"><a href="/ketua/struktur"><i class="fa fa-users"></i> <span>KELOLA STRUKTUR</span></a></li>
    
    <li class="{{ request()->is('ketua/pendaftar') ? 'active' : ''}}"><a href="/ketua/pendaftar"><i class="fa fa-book"></i> <span>DATA PENDAFTAR</span></a></li>


    {{-- KELOLA USER --}}
    <li class="header">KELOLA USER</li>

    
    <li class="{{ request()->is('ketua/user') ? 'active' : ''}}"><a href="/ketua/user"> <i class="fa fa-dashboard"></i> <span>DATA USER</span></a></li>
  </ul>
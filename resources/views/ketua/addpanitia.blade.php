@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Panitia Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data panitia program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/panitia/store" method="POST" enctype="multipart/form-data">

    @csrf
    <table class="table" id="table-addpanitia">
        <thead class="table" style="background-color: #d16908"  >
            <tr>
            <th style="color: white">PROKER</th>
            <th style="color: white">NAMA USER</th>
            <th style="color: white">JABATAN KEPANITIAAN</th>
            <th style="color: white">DIVISI</th>
            <th style="color: white">AKSI</th>
        </tr>
    </thead>
    <tbody id="panitia_formtambah">
        <tr id="panitiaRow0">
            <td><select name="id_user[]" class="form-control user2">
                @forelse ($user as $item)
                   <option value="{{$item->id}}">{{$item->name}}</option>
                   @empty
                   <option value="0">data kosong</option>
                @endforelse
            </select></td>
            <td><select name="id_proker[]" class="form-control proker2">
                @forelse ($proker as $item)
                   <option value="{{$item->id}}">{{$item->nama_proker}}</option>
                   @empty
                   <option value="0">data kosong</option>
                @endforelse
            </select> </td>
            <td> <select class="form-control" name="jabatan[]">
                <option selected disabled> -- PILIH JABATAN --
                </option>
                <option value="KOORDINATOR"> KOORDINATOR
                </option>
                <option value="ANGGOTA"> ANGGOTA
                </option>
            </select> </td>
            <td>  <input type="text" name="divisi[]" class="form-control">
             </td>
             <td><button class="btn btn-danger btn-sm rmRow" data-id="0"><i class="fa fa-trash"></i></button> </td>
        </tr>
    </tbody>    
</table>
<div class="form-group">
    <button class="btn btn-warning btn-sm" id="btn_addForm" style="margin-left: 8pt;"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button> 
    <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;SIMPAN</button>
    </div>
</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    var count = 1;

    $(document).ready(function() {
        $(".user2").select2({
            width: '100%'
        });
        $(".proker2").select2({
            width: '100%'
        });
    });

    $('#btn_addForm').click(function (e) { 
        e.preventDefault();
        var content =  '<tr id="panitiaRow'+count+'"><td><select name="id_user[]" class="form-control user2">';
            content += '@forelse ($user as $item)<option value="{{$item->id}}">{{$item->name}}</option>@empty<option value="0">data kosong</option>@endforelse';
            content += '</select></td><td><select name="id_proker[]" class="form-control proker2">';
            content += '@forelse ($proker as $item)<option value="{{$item->id}}">{{$item->nama_proker}}</option>@empty<option value="0">data kosong</option>@endforelse';
            content += '</select></td><td><select class="form-control" name="jabatan[]">';
            content += '<option selected disabled> -- PILIH JABATAN --</option><option value="KOORDINATOR"> KOORDINATOR</option><option value="ANGGOTA"> ANGGOTA</option>';
            content += '</select></td><td><input type="text" name="divisi[]" class="form-control"></td>';
            content += '<td><button class="btn btn-danger btn-sm rmRow" data-id="'+count+'"><i class="fa fa-trash"></i></button></td></tr>';
        $('#panitia_formtambah').append(content);
        $(".proker2").select2({
                width: '100%'
        });
        $(".user2").select2({
            width: '100%'
        });            
        count++;
    });

    $(document).on('click','.rmRow', function (e) { 
        e.preventDefault();
        var id = $(this).data('id');
        // console.log(id);
        $('#panitiaRow'+id).remove();
        
    });
</script>

@endsection
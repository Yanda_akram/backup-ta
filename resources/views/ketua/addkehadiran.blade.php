@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Kehadiran</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data kehadiran</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/kehadiran/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        
        <div class="row">   
            <div class="col-sm-12">
            <div class="form-group">
                <label>NAMA USER</label>
                <input type="text" name="id_user" class="form-control">
            </div>
            @if($errors->has('id_user'))
                <div class="text-danger">
                    {{ $errors->first('id_user')}}
                </div>
             @endif
             
             <div class="form-group">
                 <label>ID RAPAT</label>
                 <input type="text" name="id_rapat" class="form-control">
             </div>
             @if($errors->has('id_rapat'))
                 <div class="text-danger">
                     {{ $errors->first('id_rapat')}}
                 </div>
              @endif
            

        </div>
        
        
        @if(Session::has(''))
            <script>
                toasts.success("{!! Session::get('') !!}");
            </script>
        @endif
            
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
        </div>
</form>


<script>
    $(document).ready(function() {
        $(".pengajuan").select2({
            width: '100%'
        });
        
    });
</script>
@endsection
@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE PENGAJUAN</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($pengajuan as $p)
    <form class="col-md-12" action="/ketua/pengajuan/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_user" class="form-label"><b>ID USER</b></label>
          {{-- <input type="text" class="form-control" name="id_user" value="{{ $p->id_user }}" > --}}

          <select class="editpengajuan" name="id_user" class="form-control">
            @forelse ($user as $item)
               <option value="{{$item->id}}" {{ ($item->id == $p->id_user)? 'selected' : ''}}>{{$item->name}}</option>
               @empty
               <option value="0">data kosong</option>
            @endforelse
        </select>
        
      
        
          <label for="nama_proker" class="form-label"><b>PROGRAM KERJA</b></label>
          <input type="text" readonly class="form-control" name="nama_proker" value="{{ $p->nama_proker }}" >
       
          <label for="tempat" class="form-label"><b>TEMPAT</b></label>
          <input type="text" readonly class="form-control" name="tempat" value="{{ $p->tempat }}" >

          <label for="detail" class="form-label"><b>DETAIL </b></label>
          <textarea class="form-control" readonly name="detail">{{ $p->detail }}</textarea>          
          
          <label for="catatan" class="form-label"><b>CATATAN</b></label>
          <textarea class="form-control" name="catatan">{{ $p->catatan }}</textarea>          
       
         
        </div>
      
        <div class="form-group col-sm-6">
          <label for="status" class="form-label"><b>STATUS</b></label>
          <select class="form-control" name="status">
            <option selected disabled> -- PILIH STATUS --
            </option>
            <option value="DIPROSES"> DIPROSES
            </option>
            <option value="DITERIMA"> DITERIMA
            </option>
            <option value="DITOLAK"> DITOLAK
            </option>
        </select>
      
          <label for="keterangan" class="form-label"><b>KETERANGAN</b></label>
          <input type="text" class="form-control" disabled name="keterangan_v" placeholder="DIPROSES PEMBIMBING">
          <input type="hidden" class="form-control" name="keterangan" value="DIPROSES">

          <label for="sumber_dana" class="form-label"><b>SUMBER DANA</b></label>
          <select class="form-control" name="sumber_dana">
            <option selected disabled> -- PILIH SUMBER DANA --
            </option>
            <option value="KAS BEI"> KAS BEI
            </option>
            <option value="SPONSORSHIP"> SPONSORSHIP
            </option>
            <option value="KAMPUS"> KAMPUS
            </option>
            <option value="LAINNYA"> LAINNYA
            </option>
        </select>
        
          <label for="anggaran" class="form-label"><b>ANGGARAN</b></label>
          <input type="text"  class="form-control" name="anggaran" value="{{ $p->anggaran }}" >

        

        </div>
        
      </div>
      <br>
      <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
      <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".editpengajuan").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection
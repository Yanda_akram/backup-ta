@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE PROKER</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($proker as $p)
    <form class="col-md-12" action="/ketua/proker/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_user" class="form-label"><b>ID PENGAJUAN</b></label>
          <select class="pengajuan" name="id_pengajuan" class="form-control">
            @forelse ($pengajuan as $item)
               <option value="{{$item->id}}" {{ ($item->id == $p->id_pengajuan)? 'selected' : ''}}>{{$item->nama_proker}}</option>
               @empty
               <option value="0">data kosong</option>
            @endforelse
        </select>
       
         
        </div>
      
        <div class="form-group col-sm-6">
          
        
          <label for="tanggal" class="form-label"><b>TANGGAL</b></label>
          <input type="date" class="form-control" name="tanggal" value="{{ $p->tanggal }}" >
        
     
          <label for="status" class="form-label"><b>STATUS PROKER</b></label>
          <select class="form-control" name="status">
            <option selected disabled> -- PILIH STATUS --
            </option>
            <option value="TERLAKSANA"> TERLAKSANA
            </option>
            <option value="BELUM TERLAKSANA"> BELUM TERLAKSANA
            </option>
        </select>
    </div>
</div>
<br>
<button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
<a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".pengajuan").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection
@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE USER</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda memvalidasi status yang disediakan form. . . .
</div>
{{-- Alert --}}

    @foreach($user as $u)
    <form class="col-md-12" action="/ketua/user/edit" method="post">
    {{ csrf_field() }} 
    <div class="hidden">
      <label for="id" class="form-label"><b></b></label>
      <input type="hidden" class="form-control" name="id" value="{{ $u->id }}" >
    </div> 
      <div class="row">
        <div class="form-group col-sm-12">
          <label for="name" class="form-label"><b>NAMA</b></label>
          <input type="text" class="form-control" name="name" value="{{ $u->name }}" >

          <label for="username" class="form-label"><b>username</b></label>
          <input type="text" class="form-control" name="username" value="{{ $u->username }}" >

          <label for="password" class="form-label"><b>password</b></label>
          <input type="text" class="form-control" name="password" value="{{ $u->password }}" >
          
          <label for="level" class="form-label"><b>Level</b></label>
          <input type="text" class="form-control" name="level" value="{{ $u->level }}" >

        </div>
      
      
    </div>
    <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
    <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
      
    </form>
    @endforeach
    
    {{-- <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script> --}}
@endsection